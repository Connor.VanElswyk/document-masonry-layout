const path = require('path')

export default {
    root: path.resolve(__dirname, 'src'),
    resolve: {
        alias: {
            '~bootstrap': path.resolve(__dirname, 'node_modules/bootstrap'),
            '~feather-icons': path.resolve(__dirname, 'node_modules/feather-icons'),
        }
    },
    server: {
        port: 8080,
        hot: true
    }
}
