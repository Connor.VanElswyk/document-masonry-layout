import '../scss/styles.scss'
import './plugins'
import './listeners'
import './documents'
import './patient'

// import { mdiFilePdfBox } from '@mdi/js'
import { mdiPinOutline } from '@mdi/js';

document.querySelector('title').innerText = import.meta.env.VITE_APP_NAME // App Name text
document.querySelector('#brand-anchor').innerText = import.meta.env.VITE_APP_NAME // App Name text
document.querySelector('#current-year').innerText = new Date().getFullYear().toString() // Copyright Year text
