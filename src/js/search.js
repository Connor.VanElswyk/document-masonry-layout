const e = document.querySelector('#search-input')

export const focus = () => e.focus({focusVisible: true})
export const blur = () => e.blur()

