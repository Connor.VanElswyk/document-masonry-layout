import {entry} from "./documents.json";

const docs = []
for (const entity of entry) {

    const resource = entity['resource']

    let loinc = null, category = null
    for (const code of resource.category) {

        const coding = code['coding']
        if (!coding || coding.length === 0) {
            continue
        }

        let c = coding[0]
        if (c.code === "unknown") continue
        if (c.display && category == null) {
            category = c.display
        }
        if (c.system.includes('loinc.org')) {
            loinc = c.code
        }
    }

    const contributorMap = new Map()
    const authors = resource['author']
    if (authors) {
        for (const author of authors) {
            const key = author.display
            if (key === 'Scanned Document' || contributorMap.has(key)) {
                continue
            }
            const role = author.reference.split("/")[0]
            contributorMap.set(key, role)
        }
    }
    const authenticator = resource['authenticator']
    if (authenticator) {
        const key = authenticator.display
        if (key !== 'Scanned Document' && !contributorMap.has(key)) {
            const role = authenticator.reference.split("/")[0]
            contributorMap.set(key, role)
        }
    }

    let sources = []
    for (const content of resource.content) {
        const attachment = content['attachment']
        sources.push({
            contentType: attachment.contentType,
            url: attachment.url,
            title: attachment.title,
        })
    }

    let title = resource.type.text
    let subtitle = ''
    if (title) {
        title = title
            .replace('- Text', '')
            .replace('-Text', '')
            .replace(' and ', ' & ')
            .replace('Note', '')
            .replace('PDF ', '')
            .replace('Documentation', '')
            .replace('Examination', 'Exam')
            .replace('(Shared)', '')
            .replace('(TISSU)', '(TISSUE)')
            .replace('Physical Examination Documentation', 'Physical Exam')

        if (title.includes('-')) {
            const chunks = title.split('-')
            title = chunks[0]
            subtitle = chunks[1]
        } else {
            for (const suffix of ['(BM)', '(PB)', '(TISSUE)']) {
                if (title.includes(suffix)) {
                    title = title.replace(suffix, '')
                    subtitle = suffix
                    break
                }
            }
        }

        subtitle = subtitle
            .replace('(', ' ')
            .replace(')', '')
            .trim()

        title = title.trim()
    }

    let contributors = null
    if (contributorMap.size === 1) {
        const next = contributorMap.entries().next()
        contributors = {name:next.value[0], role:next.value[1]}
    } else {
        contributors = []
        contributorMap.forEach((name, role) => contributors.push({name, role}))
    }

    let id = resource.id
    let updated = new Date(resource.meta.lastUpdated).toLocaleDateString('en-US', {year:'2-digit', month:'2-digit', day:'2-digit'})

    let doc = {id, title, subtitle, updated, loinc, category, contributors, sources}

    docs.push(doc)
}

const divs = []
for (const doc of docs) {

    divs.push(` 
<div class="card mx-auto mb-2">
    <div class="card-body">
        <p class="card-text small">
            ${doc.title}
            <br>
            ${doc.subtitle ? `<small class="text-muted">${doc.subtitle}</small>` : '<br>'}                    
        </p>
        <p class="card-text small">
            <small class="text-muted">
                ${doc.updated}
            </small>
        </p>
    </div>
</div>`)
}
document.querySelector('#main-div').innerHTML = divs.join('')