import {blur, focus} from './search'

const listInput = document.querySelector('#btnradio1')
const gridInput = document.querySelector('#btnradio2')

const keyboardListener = async e => {
    const b = e.key.match(/Arrow(Left|Right)/)
    console.log(b)
    switch (e.key) {
        case '/':
            e.preventDefault()
            focus()
            return
        case 'ArrowLeft':
            e.preventDefault()
            listInput.click()
            return
        case 'ArrowRight':
            e.preventDefault()
            gridInput.click()
            return
        case 'Escape':
            e.preventDefault()
            blur()
            listInput.blur()
            gridInput.blur()
    }
}

document.addEventListener("keydown", keyboardListener);

window.oncontextmenu = e => {
    e.preventDefault()
    alert('Right Click')
}