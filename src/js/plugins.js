import {Popover} from 'bootstrap'; // Mobile Nav Element
import feather from 'feather-icons' // Primary Icon Provider

// Initialized Plugins
feather.replace({ 'aria-hidden': 'true' })
document
    .querySelectorAll('[data-bs-toggle="popover"]')
    .forEach(popover => new Popover(popover, {}))